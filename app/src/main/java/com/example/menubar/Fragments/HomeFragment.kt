package com.example.menubar.Fragments

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import com.example.menubar.R

class HomeFragment : Fragment(R.layout.fragment_home) {
    private lateinit var amountEditText: EditText
    private lateinit var sendButton:Button

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        amountEditText = view.findViewById(R.id.editTextArgument)
        sendButton = view.findViewById(R.id.buttonSend)

        val navController = Navigation.findNavController(view)
        sendButton.setOnClickListener{

            val nameText = amountEditText.text.toString()

            if(nameText.isEmpty()){
                return@setOnClickListener
            }

            val action = HomeFragmentDirections.actionHomeFragmentToDashboardFragment(nameText)

            navController.navigate(action)


        }
    }




}