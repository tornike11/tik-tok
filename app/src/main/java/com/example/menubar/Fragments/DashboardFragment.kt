package com.example.menubar.Fragments

import android.net.Uri
import android.os.Bundle
import android.text.Html
import android.view.View
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.menubar.R

class DashboardFragment : Fragment(R.layout.fragment_dashboard) {
    private lateinit var amountEditText1: EditText
    private lateinit var sendButton1: Button
    val tiktokerebi = arrayOf("Merab Kvinikadze", "Guram Kveniashvili", "Giorgi Gajishvili","Tornike Elkanishvili")
    var videoView: VideoView? = null
    var mediaController: MediaController? = null



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        amountEditText1 = view.findViewById(R.id.editTextArgument1)
        sendButton1 = view.findViewById(R.id.buttonSend1)
        val navController = Navigation.findNavController(view)
        videoView = view.findViewById(R.id.videoView)


        val nameText1 = DashboardFragmentArgs.fromBundle(requireArguments()).amount.toString()
        if(tiktokerebi.contains(nameText1)){
            Toast.makeText(context, Html.fromHtml("<font color='#90EE90' ><b>" + "მომხმარებელი ნაპოვნია" + "</b></font>"), Toast.LENGTH_SHORT).show()
            if(mediaController == null){
                mediaController = MediaController(context)
                mediaController!!.setAnchorView(this.videoView)
            }
            videoView!!.setMediaController(mediaController)
            videoView!!.setVideoURI(Uri.parse("android.resource://" + activity?.getPackageName() + "/" + R.raw.gajo))
            videoView!!.requestFocus()
            videoView!!.start()
        }
        sendButton1.setOnClickListener {

            val nameText = amountEditText1.text.toString()

            if (nameText.isEmpty()) {
                return@setOnClickListener
            }
            else if(tiktokerebi.contains(nameText)){
                Toast.makeText(context, Html.fromHtml("<font color='#90EE90' ><b>" + "მომხმარებელი ნაპოვნია" + "</b></font>"), Toast.LENGTH_SHORT).show()
                if(mediaController == null){
                    mediaController = MediaController(context)
                    mediaController!!.setAnchorView(this.videoView)

                }
                videoView!!.setMediaController(mediaController)
                videoView!!.setVideoURI(Uri.parse("android.resource://" + activity?.getPackageName() + "/" + R.raw.gajo))
                videoView!!.requestFocus()
                videoView!!.start()

            }else{
                Toast.makeText(context, Html.fromHtml("<font color='#FF0000' ><b>" + "მომხმარებელი არაა ნაპოვნია" + "</b></font>"), Toast.LENGTH_SHORT).show()
            }

        }
    }
}