package com.example.menubar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.MediaController
import android.widget.TextView
import android.widget.VideoView
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.menubar.Fragments.DashboardFragment
import com.example.menubar.Fragments.DashboardFragmentArgs
import com.google.android.material.bottomnavigation.BottomNavigationView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        /*val mediaController = MediaController(this)
        mediaController.setAnchorView(DashboardFragment.videoShow)*/



        val navView = findViewById<BottomNavigationView>(R.id.bottomNavMenu)

        val controller = findNavController(R.id.nav_host_fragment)
        navView.itemIconTintList = null

        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.dashboardFragment,
                R.id.homeFragment,
                R.id.notificationFragment
            )
        )
        setupActionBarWithNavController(controller,appBarConfiguration)
        navView.setupWithNavController(controller)


    }
}